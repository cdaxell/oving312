// @flow

import Sequelize from 'sequelize';
import type { Model } from 'sequelize';

let sequelize = new Sequelize('cdaxell', 'cdaxell', 'yAmB12A4', {
  host: process.env.CI ? 'mysql' : 'mysql.stud.iie.ntnu.no', // The host is 'mysql' when running in gitlab CI
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

export let Posts: Class<
    Model<{ postId?: number, title: string, date: number, content: string, image: string, uppVotes: number, nickName: string, relevance: number, categoryId: number }>
    > = sequelize.define('Posts', {
    postId: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    title: Sequelize.STRING,
    date: Sequelize.BIGINT,
    content: Sequelize.STRING,
    image: Sequelize.STRING,
    uppVotes: Sequelize.INTEGER,
    nickName: Sequelize.STRING,
    relevance: Sequelize.INTEGER,
    categoryId: Sequelize.STRING
});

export let Comments: Class<
    Model<{ commentId?: number, postId?: number, nickName: string, text: string, uppVote: number, parent: number}>
    > = sequelize.define('Comments', {
    commentId: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    postId: { type: Sequelize.INTEGER, foreignKey: true},
    nickName: Sequelize.STRING,
    text: Sequelize.STRING,
    uppVote: {type: Sequelize.INTEGER, default: 0},
    parent: Sequelize.INTEGER
});

export let Categories: Class<
    Model<{ categoryId?: number, name: number, uppVote: number}>
    > = sequelize.define('Categories', {
    categoryId: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    name: Sequelize.STRING,
    uppVote: Sequelize.INTEGER
});


// Drop tables and create test data when not in production environment
let production = process.env.NODE_ENV === 'production';
// The sync promise can be used to wait for the database to be ready (for instance in your tests)
//export let sync = sequelize.sync({ force: production ? false : true }).then(() => {
export let sync = sequelize.sync({ force: !production }).then(() => {
  if (!production)
    return Posts.create({
        title: 'The Pope is the boooss',
        date: 201811161144,
        content: 'All is said in the title.',
        image: 'https://i.kinja-img.com/gawker-media/image/upload/s--isTTVY5o--/c_scale,f_auto,fl_progressive,q_80,w_800/1446462109604562863.png',
        uppVotes: 6,
        nickName: 'Francy',
        relevance: 0,
        categoryId: "Pope Meme"
    }).then(() =>
       Posts.create({
          title: 'John Cena is a templar!',
          date: 201811161143,
          content: 'John Cena has decleared his independence from the ring and will join us in taking back israel! Deus Vult!',
          image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/John_Cena_May_2016.jpg/800px-John_Cena_May_2016.jpg ',
          uppVotes: 2,
          nickName: 'Johny',
          relevance: 0,
          categoryId: "Famus Templars"
      }).then(()=>
       Categories.create({
           name: 'Israel',
           uppVote: 0,
       })).then(()=>
       Categories.create({
           name: 'Deus Vult',
           uppVote: 100
       }).then(()=>{
       Categories.create({
           name: 'Famus Templars',
           uppVote: 100
       }).then(()=>{
           Categories.create({
               name: 'Third Crusade',
               uppVote: 100
           })
       }).then(()=>{
           Categories.create({
               name: 'Pope Meme',
               uppVote: 100
           })
       }).then(()=>{
           Posts.create({
               title: 'We will take back Israel',
               date: 201811161143,
               content: 'We have lanched the 4 crusade to take back israel! DEUS VULT',
               image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Israel.svg/660px-Flag_of_Israel.svg.png',
               uppVotes: 2,
               nickName: 'Magnus',
               relevance: 1,
               categoryId: "Israel"
           })
       })
       }))
    );
});
