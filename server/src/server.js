// @flow

import express from 'express';
import path from 'path';
import reload from 'reload';
import fs from 'fs';
import { Posts } from './models.js';
import { Comments } from './models.js';
import { Categories } from './models.js';

type Request = express$Request;
type Response = express$Response;

const public_path = path.join(__dirname, '/../../client/public');

let app = express();

app.use(express.static(public_path));
app.use(express.json()); // For parsing application/json

app.get('/post/:postId', (req: Request, res: Response) => {
    return Posts.findOne({
        where: {postId: Number(req.params.postId)}
    }).then(post => (post ? res.send(post) : res.sendStatus(404)));
});

app.get('/posty/', (req: Request, res: Response) => {
    return Posts.findAll().then(post => (post ? res.send(post) : res.sendStatus(404)));
});

app.get('/relevance/:relevance/date/:date/offset/:offset/limit/:limit', (req: Request, res: Response) => {
    return Posts.findAll({
        where: {$and: [{relevance: Number(req.params.relevance)}, {date: {$gte: Number(req.params.date)}}]},
        order: [['uppVotes', 'DESC'],['date', 'DESC']],
        limit: Number(req.params.limit),
        offset: Number(req.params.offset),
    }).then(post => (post ? res.send(post) : res.sendStatus(404)));
});

app.get('/categoryId/:categoryId/date/:date/offset/:offset/limit/:limit', (req: Request, res: Response) => {
    return Posts.findAll({
        where: {$and: [{categoryId: req.params.categoryId}, {date: {$gte: Number(req.params.date)}}]},
        order: [['uppVotes', 'DESC'],['date', 'DESC']],
        limit: Number(req.params.limit),
        offset: Number(req.params.offset),
    }).then(post => (post ? res.send(post) : res.sendStatus(404)));
});

app.delete('/title/:title/date/:date', (req: Request, res: Response) => {
    return Posts.destroy({
        where: {$and: [{title: req.params.title}, {date: Number(req.params.date)}]}
    }).then(count => (count ? res.sendStatus(200) : res.sendStatus(404)));
});

app.delete('/posts/:postId', (req: Request, res: Response) => {
    return Posts.destroy({
        where: {postId: Number(req.params.postId)}
    }).then(count => (count ? res.sendStatus(200) : res.sendStatus(404)));
});

app.put('/posts', (req: Request, res: Response) => {
    if (
        !req.body ||
        typeof req.body.postId != 'number' ||
        typeof req.body.title != 'string' ||
        typeof req.body.content != 'string' ||
        typeof req.body.image != 'string' ||
        typeof req.body.uppVotes != 'number' ||
        typeof req.body.nickName != 'string' ||
        typeof req.body.relevance != 'number' ||
        typeof req.body.categoryId != 'number'
    )
        return res.sendStatus(400);

    return Posts.update(
        { title: req.body.title, content: req.body.content, image: req.body.image, relevance: req.body.relevance, categoryId: req.body.categoryId, nickName: req.body.nickName },
        { where: { id: req.body.postId } }
    ).then(count => (count ? res.sendStatus(200) : res.sendStatus(404)));
});

app.post('/posts', (req: Request, res: Response) => {
    if (
        !req.body ||
        typeof req.body.title != 'string' ||
        typeof req.body.content != 'string' ||
        typeof req.body.image != 'string' ||
        typeof req.body.nickName != 'string' ||
        typeof req.body.categoryId != 'string' ||
        typeof req.body.relevance != 'number'
    ) return res.sendStatus(400);

    return Posts.create(
        { title: req.body.title, content: req.body.content, image: req.body.image, relevance: req.body.relevance,
            categoryId:  req.body.categoryId, nickName: req.body.nickName, date: nowDate(), uppVote: '0'     }
    ).then(count => (count ? res.sendStatus(200) : res.sendStatus(404)));
});

function nowDate(){
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth()+1; //January is 0!
    let yyyy = today.getFullYear();
    let hh = today.getHours();
    let min = today.getMinutes();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }

    today = yyyy+''+mm+''+dd+''+hh+''+mm;
    return parseInt(today);
}

/////////////////////////////////////////////////////

app.get('/category/:categoryId', (req: Request, res: Response) => {
    return Categories.findOne({
        where: {categoryId: Number(req.params.categoryId)}
    }).then(post => (post ? res.send(post) : res.sendStatus(404)));
});

app.get('/category/', (req: Request, res: Response) => {
    return Categories.findAll().then(post => (post ? res.send(post) : res.sendStatus(404)));
});

app.get('/name/:name', (req: Request, res: Response) => {
    return Categories.findOne({
        where: {name: req.params.name}
    }).then(number => (number ? res.send(number) : res.sendStatus(404)));
});


app.delete('category/:categoryId', (req: Request, res: Response) => {
    return Categories.destroy({
        where: {categoryId: Number(req.params.categoryId)}
    }).then(count => (count ? res.sendStatus(200) : res.sendStatus(404)));
});

//////////////////////////////////////////////////

app.put('/category', (req: Request, res: Response) => {
    if (
        !req.body ||
        typeof req.body.name != 'string' ||
        typeof req.body.uppVote != 'number'
    )
        return res.sendStatus(400);

    return Categories.update(
        { name: req.body.name, uppVote: req.body.uppVote},
        { where: { id: req.categoryId } }
    ).then(count => (count ? res.sendStatus(200) : res.sendStatus(404)));
});

app.post('/category', (req: Request, res: Response) => {
    if (
        !req.body ||
        typeof req.body.name != 'string'
    )
        return res.sendStatus(400);

    return Categories.build(
        { name: req.body.name, uppVote: 0}
    ).save().then(count => (count ? res.sendStatus(200) : res.sendStatus(404)));
});


// Hot reload application when not in production environment
if (process.env.NODE_ENV !== 'production') {
  let reloadServer = reload(app);
  fs.watch(public_path, () => reloadServer.reload());
}

// The listen promise can be used to wait for the web server to start (for instance in your tests)
export let listen = new Promise<void>((resolve, reject) => {
  app.listen(3000, error => {
    if (error) reject(error.message);
    console.log('Server started');
    resolve();
  });
});
