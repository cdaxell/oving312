// @flow
import axios from 'axios';
axios.interceptors.response.use(response => response.data);

class Student {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
}

class StudentService {
  getStudents(): Promise<Student[]> {
    return axios.get('/students');
  }

  getStudent(id: number): Promise<Student> {
    return axios.get('/students/' + id);
  }

  updateStudent(student: Student): Promise<void> {
    return axios.put('/students', student);
  }
}

class Post {
    constructor(title: string, content: string, image: string, nickname: string, relevance: number, categoryId: number){
        this.title = title;
        this.content = content;
        this.image = image;
        this.nickName = nickname;
        this.relevance = relevance;
        this.categoryId = categoryId;

    }
    postId: number;
    title: string;

    date: number;
    content: string;

    image: string;

    uppVotes: number;
    nickName: string;

    relevance: number;
    categoryId: number;

    createdAt: number;
}

class Comment{

}

class Category{
    categoryId: number;
    name: string;
    uppVote: number;
}


class PostService {
    getPost(id: number): Promise<Post>{
        return axios.get('/posts/' + id);
    }

    getPosts(): Promise<Post[]>{
        return axios.get('/posty/');
    }

    getPostsRel(relev: number, date: number, offset: number, limit: number): Promise<Post[]>{
        return axios.get('/relevance/'+relev+'/date/'+date+'/offset/'+offset+'/limit/'+limit);
    }

    getPostsCat(categoryId: number, date: number, offset: number, limit: number): Promise<Post[]>{
        return axios.get('/categoryId/'+categoryId+'/date/'+date+'/offset/'+offset+'/limit/'+limit);
    }
    deletePost(title: string, date: number): Promise<void>{
        return axios.delete('/title/'+title+'/date/'+date);
    }

    deletePostId(id: number): Promise<void>{
        return axios.delete('/posts/' + id);
    }

    updatePost(post: Post): Promise<void>{
        return axios.put('/posts/', post);
    }

    createPost(post: Post): Promise<void>{
        return axios.post('/posts/', post);
    }

    getCategory(categoryId: number): Promise<Category>{
        return axios.get('/category/' + categoryId);
    }

    getCategoryName(name: string): Promise<Category>{
        return axios.get('/name/' + name);
    }

    getCategories(): Promise<Category[]>{
        return axios.get('/category/');
    }

   deleteCategories(categoryId: number): Promise<void>{
        return axios.delete('/category/' + categoryId);
    }

    putCategories(category: Category): Promise<void>{
        return axios.put('/category/', category);
    }

    postCategories(category: Category): Promise<void>{
        return axios.post('/category/', category);
    }
}

export let studentService = new StudentService();
export let postService = new PostService();
