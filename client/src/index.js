// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { postService, studentService } from './services';
import { Component } from 'react-simplified';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import { Alert } from './widgets';

if (process.env.NODE_ENV !== 'production') {
    let script = document.createElement('script');
    script.src = '/reload/reload.js';
    if (document.body) document.body.appendChild(script);
}

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();

//dateformat rounds of to minute

class Home extends Component {
    render() {
        return (
            <div id="home">
                <Alert />
                <div id='blackbar'></div>
                <MainContent />
            </div>
        );
    }
}

class Navbar extends Component {
    render() {
        return (
            <nav id="navbar" className="navbar navbar-light bg-light">
                <a id="navbar-title" className="navbar-light" onClick={() => this.goBack()}>
                    The Cursades
                </a>
                <img className="img-fluid" id="deusVult" onClick={() => this.goBack()} src={"https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Cross-Pattee-Heraldry.svg/600px-Cross-Pattee-Heraldry.svg.png"} />
                <form className="form-inline">
                    <NavLink to="/register">
                        <button className="btn btn-dark" type="button">
                            New Crusade!
                        </button>
                    </NavLink>
                </form>
            </nav>
        );
    }

    goBack() {
        history.push('/');
        window.location.reload();
    }
}

class Live extends Component {
    posts = [];



    render() {
        return (
            <div id="live" className="row text-center">
                {this.posts.map(post => (
                    <NavLink id="link" key={post.postId} to={'/post/' + post.postId}>
                        <div className="feed-item">{post.title + ' ' + dateToDate(post.createdAt) + ''}
                        </div>
                    </NavLink>
                ))}
            </div>
        );
    }

    rollin() {
        $(document).ready(function() {
            $('#live').slick({
                pauseOnHover: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplaySpeed: 10,
                autoplay: true,
                speed: 10,
                arrows: false
            });
        });
    }

    mounted() {
        postService
            .getPostsRel(1, 0, 0, 5)
            .then(posts => ((this.posts = posts), this.rollin()))
            .catch((error: Error) => console.log(error.message));
    }
}

class MainContent extends Component {
    posts = [];
    categories = [];
    showing = [];
    filterOpen = false;

    render() {
        return (
            <div id="main-content-wrapper">
                <Live />

                <div id="catMenu" className="card-header">
                    {this.categories.map(category => (
                            <button id="subClassMenu" key={category.categoryId} href="#" className="btn btn-light" type="button" onClick={() => this.filter(category.name)}>
                                {category.name}
                            </button>
                    ))}
                </div>

                <div id="main-content">
                    {this.showing.map(post => (
                        <Post
                            postId={post.postId}
                            title={post.title}
                            date={post.createdAt}
                            image={post.image}
                            categoryId={post.categoryId}
                        />
                    ))}
                </div>

            </div>
        );
    }

    filter(category) {
        postService.getPosts().then(posts => ((this.posts = posts)));
        let cap = 20;
        console.log(category);
        this.showing = this.posts.filter(post => post.categoryId == category && cap-- > 0);
    }

    mounted() {
        let cap = 20;
        postService
            .getPostsRel(1, 0, 0, 20)
            .then(
                posts => (
                    (this.posts = posts), (this.showing = posts.filter(post => cap-- > 0 && post.relevance >= 0))
                ),
                postService
                    .getCategories()
                    .then(categories => (this.categories = categories))
                    .catch((error: Error) => console.log(error.message))
            )
            .catch((error: Error) => console.log(error.message));
    }
}

class Post extends Component<{ postId: number, title: string, date: Date, image: String, category: string }> {
    optionsOpen = false;

    render() {
        return (
            <div className="card post" id="postCard">
                <img className="card-img-top post-image" id = "yoman" src={this.props.image} alt="Card image" />
                <div className="card-body post-body">
                    <NavLink to={'/post/' + this.props.postId}>
                        <h5 className="card-title post-title">{this.props.title}</h5>
                    </NavLink>
                    <h6 className="card-text post-category">{this.props.category}</h6>
                    <h6 className="card-text post-date">{dateToDate(this.props.date)}</h6>

                    <div>
                        <button onClick={()=>this.deleteThis(this.props.postId)} className="btn btn-dark" type="button">
                            Delete This
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    deleteThis(postId) {
        postService
            .deletePostId(postId)
            .then(window.location.reload())
            .catch((error: Error) => Alert.danger(error.message));
    }
}

class PostInfo extends Component<{ match: { params: { id: number } } }> {
    post: Post = null;

    render() {
        //if (this.post==null) return null;
        return (
            <div id="post">
                <img id="post" src={this.post.image} alt="Post front image" />
                <div id="post">
                    <p id="post"> {postService.getCategory(this.post.categoryId)} </p>
                    <p id="post">{this.post.title}</p>
                    <p> ({dateToDate(this.post.createdAt)}) </p>
                </div>
                <div id="post">
                    <p>{this.post.content}</p>
                </div>
            </div>
        );
    }

    mounted() {
        postService
            .getPost(this.match.params.id)
            .then(post=> (this.post = post))
            .catch((error: Error) => console.log(error.message));
    }
}

class PostEdit extends Component<{ match: { params: { id: number } } }> {
    post = null;
    categories = [];

    render() {
        if (!this.post) return null;

        return (
            <div id="register">
                <Alert />
                <div id="register-wrapper">
                    <div id="register-form" className="card">
                        <form style={{ margin: '10px' }}>
                            <div style={{ textAlign: 'center' }}>
                                <h5 className="card-title">Edit post</h5>
                            </div>
                            <div className="form-group">
                                <label style={{ float: 'left' }}>Title</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={this.post.title}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.post.title = event.target.value.trim())
                                    }
                                />
                            </div>
                            <div className="form-group">
                                <label style={{ float: 'left' }}>Picture URL</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={this.post.image}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.post.image = event.target.value.trim())
                                    }
                                />
                            </div>
                            <div className="form-group">
                                <label>Text</label>
                                <textarea
                                    className="form-control"
                                    rows="10"
                                    value={this.post.content}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.post.content = event.target.value.trim())
                                    }
                                />
                            </div>

                            <label>Category</label>
                            <select
                                className="selectpicker browser-default custom-select"
                                defaultValue={this.post.category}
                                onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                    (this.post.category = event.target.value)
                                }
                            >
                                {this.categories.map(category => (
                                    <option key={category.id} value={category.type}>
                                        {category.type}
                                    </option>
                                ))}
                            </select>
                            <label style={{ marginTop: '20px' }}>Importance</label>
                            <br />
                            <div className="form-check importance">
                                <input
                                    className="form-check-input"
                                    type="radio"
                                    name="exampleRadios"
                                    value="1"
                                    checked={this.post.relevance == 1 ? 'checked' : ''}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.post.relevance = Number(event.target.value))
                                    }
                                />
                                <label className="form-check-label">Important</label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="radio"
                                    name="exampleRadios"
                                    value="2"
                                    checked={this.post.relevance == 2 ? 'checked' : ''}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.post.relevance = Number(event.target.value))
                                    }
                                />
                                <label className="form-check-label">Less Important</label>
                            </div>
                            <button
                                type="button"
                                className="btn btn-primary"
                                style={{ marginTop: '20px' }}
                                onClick={() => this.save()}
                            >
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    async save() {
        var valid = true;
        if (this.post.title == '') {
            valid = false;
            Alert.danger('Title required');
        } else if (this.post.length > 64) {
            valid = false;
            Alert.danger('Max title characters: 64');
        }
        if (this.post.category.trim() == '') {
            valid = false;
            Alert.danger('Category required');
        }
        if (this.post.relevance != 1 && this.post.relevance != 2) {
            valid = false;
            Alert.danger('Importance required');
        }

        if (valid) {
            if (this.post.image.trim() == '') this.post.image = 'https://tinyurl.com/y73nxqn9';
            postService
                .updatePost(this.post)
                .then(history.replace('/'), window.location.reload())
                .catch((error: Error) => Alert.danger(error.message));
        }
    }

    mounted() {
        postService
            .getCategories()
            .then(
                categories => (this.categories = categories),
                postService
                    .getPost(this.props.match.params.id)
                    .then(post => (this.post = post))
                    .catch((error: Error) => console.log(error.message))
            )
            .catch((error: Error) => console.log(error.message));
    }
}

class Register extends Component {
    categories = [];
    title = '';
    image = '';
    content = '';
    category = 0;
    relevance = 0;

    render() {
        return (
            <div id="register">
                <Alert />
                <div id="register-wrapper">
                    <div id="register-form" className="card">
                        <form style={{ margin: '10px' }}>
                            <div style={{ textAlign: 'center' }}>
                                <h5 className="card-title">Register new post</h5>
                            </div>
                            <div className="form-group">
                                <label style={{ float: 'left' }}>Title</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={this.title}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.title = event.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <label style={{ float: 'left' }}>Picture URL</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={this.image}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.image = event.target.value)
                                    }
                                />
                            </div>
                            <div className="form-group">
                                <label>Text</label>
                                <textarea
                                    className="form-control"
                                    rows="10"
                                    value={this.content}
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.content = event.target.value)
                                    }
                                />
                            </div>
                            <label>Category</label>
                            <select
                                className="selectpicker browser-default custom-select"
                                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.category = event.target.value)}
                                defaultValue=""
                            >
                                <option disabled value="">
                                    {' '}
                                    -- select category --{' '}
                                </option>
                                {this.categories.map(category => (
                                    <option key={category.categoryId} value={category.name}>
                                        {category.name}
                                    </option>
                                ))}
                            </select>
                            <label style={{ marginTop: '20px' }}>Importance</label>
                            <br />
                            <div className="form-check importance">
                                <input
                                    className="form-check-input"
                                    type="radio"
                                    name="exampleRadios"
                                    value="1"
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.relevance = Number(event.target.value))
                                    }
                                />
                                <label className="form-check-label">Important</label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="radio"
                                    name="exampleRadios"
                                    value="2"
                                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                                        (this.relevance = Number(event.target.value))
                                    }
                                />
                                <label className="form-check-label">Less Important</label>
                            </div>
                            <button
                                type="button"
                                className="btn btn-primary"
                                style={{ marginTop: '20px' }}
                                onClick={() => this.register()}
                            >
                                Register
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    async register() {
        var valid = true;
        if (this.title.trim() == '') {
            valid = false;
            Alert.danger('Title required');
        } else if (this.title.trim().length > 64) {
            valid = false;
            Alert.danger('Max title characters: 64');
        }
        if (this.category.trim() == '') {
            valid = false;
            Alert.danger('Category required');
        }
        if (this.relevance != 1 && this.relevance != 2) {
            valid = false;
            Alert.danger('Importance required');
        }

        let john = new Post();
        john.relevance = this.relevance;
        john.title = this.title.trim();
        john.image = this.image.trim();
        john.categoryId = this.category;
        john.nickName = 'nigger';
        john.content = this.content.trim();

        if (valid) {
            if (john.image == '') this.image = 'https://tinyurl.com/y73nxqn9';
            postService
                .createPost(john)
                .then(history.replace('/'), window.location.reload())
                .catch((error: Error) => Alert.danger(error.message));
        }
    }

    mounted() {
        postService
            .getCategories()
            .then(categories => (this.categories = categories))
            .catch((error: Error) => console.log(error.message));
    }
}

const root = document.getElementById('root');

function dateToDate(date) {
    var dateObject = new Date(date);
    dateObject.setSeconds(0, 0);
    return dateObject
        .toISOString()
        .replace('T', ' ')
        .replace(':00.000Z', '');
}

function renderRoot() {
    if (root)
        ReactDOM.render(
            <HashRouter>
                <div id="page">
                    <Navbar />
                    <Route exact path="/" component={Home} />
                    <Route exact path="/register" component={Register} />
                    <Route exact path="/post/:id" component={PostInfo} />
                    <Route exact path="/post/:id/edit" component={PostEdit} />
                </div>
            </HashRouter>,
            root
        );
}

renderRoot();
